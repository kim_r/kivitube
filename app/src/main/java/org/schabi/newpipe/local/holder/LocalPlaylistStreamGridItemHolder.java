package org.schabi.newpipe.local.holder;

import android.view.ViewGroup;

import com.wezom.kivitube.common.IncreaseOnFocusListener;

import org.schabi.newpipe.R;
import org.schabi.newpipe.local.LocalItemBuilder;

public class LocalPlaylistStreamGridItemHolder extends LocalPlaylistStreamItemHolder {

	public LocalPlaylistStreamGridItemHolder(LocalItemBuilder infoItemBuilder, ViewGroup parent) {
		super(infoItemBuilder, R.layout.list_stream_playlist_grid_item, parent); //TODO
		itemView.setOnFocusChangeListener(new IncreaseOnFocusListener());
	}
}
