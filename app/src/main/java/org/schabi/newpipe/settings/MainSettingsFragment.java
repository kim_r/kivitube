package org.schabi.newpipe.settings;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.webkit.CookieManager;
import android.webkit.WebStorage;

import com.google.android.exoplayer2.util.Log;

import org.schabi.newpipe.BuildConfig;
import org.schabi.newpipe.CheckForNewAppVersionTask;
import org.schabi.newpipe.R;
import org.schabi.newpipe.local.subscription.SubscriptionService;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainSettingsFragment extends BasePreferenceFragment {
    public static final boolean DEBUG = !BuildConfig.BUILD_TYPE.equals("release");

    private Disposable deleteSubsDisposable;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.main_settings);

        if (!CheckForNewAppVersionTask.isGithubApk()) {
            final Preference update = findPreference(getString(R.string.update_pref_screen_key));
            getPreferenceScreen().removePreference(update);

            defaultPreferences.edit().putBoolean(getString(R.string.update_app_key), false).apply();
        }

        if (!DEBUG) {
            final Preference debug = findPreference(getString(R.string.debug_pref_screen_key));
            getPreferenceScreen().removePreference(debug);
        }

        // logout
        Preference discard = findPreference("discard_last_login");
        discard.setOnPreferenceClickListener(preference -> {
            // clear browser data
            WebStorage.getInstance().deleteAllData();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().removeAllCookies(null);
                CookieManager.getInstance().flush();
            }
            // delete subs
            SubscriptionService subsService = SubscriptionService.getInstance(requireContext());
            deleteSubsDisposable = Single.fromCallable(subsService.subscriptionTable()::deleteAll)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            i -> requireActivity().finish(), // exit
                            e -> Log.e("error", e.getMessage())
                    );
            return true;
        });
    }

    @Override
    public void onDestroy() {
        if (deleteSubsDisposable != null) deleteSubsDisposable.dispose();
        super.onDestroy();
    }
}
