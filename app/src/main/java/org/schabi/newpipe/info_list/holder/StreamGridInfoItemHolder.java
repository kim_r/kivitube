package org.schabi.newpipe.info_list.holder;

import android.view.ViewGroup;

import com.wezom.kivitube.common.IncreaseOnFocusListener;

import org.schabi.newpipe.R;
import org.schabi.newpipe.info_list.InfoItemBuilder;

public class StreamGridInfoItemHolder extends StreamMiniInfoItemHolder {

	public StreamGridInfoItemHolder(InfoItemBuilder infoItemBuilder, ViewGroup parent) {
		super(infoItemBuilder, R.layout.item_kivi_video, parent);
		itemView.setOnFocusChangeListener(new IncreaseOnFocusListener());
	}
}
