package com.wezom.kivitube.presentation.channel;

import android.os.Bundle;
import android.view.View;

import com.wezom.kivitube.presentation.main.KiviMainActivity;
import com.wezom.kivitube.utils.Navigator;

import org.schabi.newpipe.extractor.stream.StreamInfoItem;
import org.schabi.newpipe.fragments.list.channel.ChannelFragment;
import org.schabi.newpipe.util.OnClickGesture;

public class KiviChannelFragment extends ChannelFragment {

    public static KiviChannelFragment newInstance(int serviceId, String url, String name) {
        KiviChannelFragment instance = new KiviChannelFragment();
        instance.setInitialData(serviceId, url, name);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(true, false, false);
        kiviMainActivity.setDrawerEnabled(false);
    }

    @Override
    protected void initListeners() {
        super.initListeners();
        infoListAdapter.setOnStreamSelectedListener(new OnClickGesture<StreamInfoItem>() {
            @Override
            public void selected(StreamInfoItem selectedItem) {
                Navigator.openVideoDetails(
                        getFM(),
                        selectedItem.getName(),
                        selectedItem.getUrl(),
                        selectedItem.getServiceId()
                );
            }
            @Override
            public void held(StreamInfoItem selectedItem) {
                // don't show dialog
            }
        });
    }

    @Override
    protected View getListHeader() {
        View listHeader = super.getListHeader();
        headerBackgroundButton.setVisibility(View.GONE);
        headerPlayAllButton.setVisibility(View.GONE);
        headerPopupButton.setVisibility(View.GONE);
        return listHeader;
    }
}
