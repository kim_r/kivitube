package com.wezom.kivitube.presentation.search;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.wezom.kivitube.presentation.main.KiviMainActivity;
import com.wezom.kivitube.utils.Navigator;

import org.schabi.newpipe.R;
import org.schabi.newpipe.extractor.ListExtractor;
import org.schabi.newpipe.extractor.channel.ChannelInfoItem;
import org.schabi.newpipe.extractor.playlist.PlaylistInfoItem;
import org.schabi.newpipe.extractor.search.SearchInfo;
import org.schabi.newpipe.extractor.stream.StreamInfoItem;
import org.schabi.newpipe.fragments.list.search.SearchFragment;
import org.schabi.newpipe.fragments.list.search.SuggestionItem;
import org.schabi.newpipe.fragments.list.search.SuggestionListAdapter;
import org.schabi.newpipe.report.ErrorActivity;
import org.schabi.newpipe.util.OnClickGesture;

import java.util.List;

public class KiviSearchFragment extends SearchFragment {

    protected static final String ARG_SHOW_TOOLBAR = "ARG_SHOW_TOOLBAR";

    protected boolean showToolbar;

    public static KiviSearchFragment newInstance(boolean showToolbar) {
        KiviSearchFragment instance = new KiviSearchFragment();
        instance.setQuery(0, "", new String[0], "");
        Bundle args = new Bundle();
        args.putBoolean(ARG_SHOW_TOOLBAR, showToolbar);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_kivi_search, container, false);
    }

    @Override
    public void onResume() {
        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(showToolbar, showToolbar, false);
        kiviMainActivity.setDrawerEnabled(false);
        super.onResume();
        itemsList.setVisibility(View.GONE);
        searchEditText.requestFocus();
    }

    @Override
    protected void initViews(View rootView, Bundle savedInstanceState) {
        super.initViews(rootView, savedInstanceState);
        searchEditText = rootView.findViewById(R.id.search_field);
    }

    @Override
    protected void initSearchListeners() {
        super.initSearchListeners();

        // I need to hide suggestions panel after selection some of its item
        suggestionListAdapter.setListener(new SuggestionListAdapter.OnSuggestionItemSelected() {
            @Override
            public void onSuggestionItemSelected(SuggestionItem item) {
                search(item.query, new String[0], "");
                searchEditText.setText(item.query);
                // here
                hideSuggestionsPanel();
                itemsList.setVisibility(View.VISIBLE);
                itemsList.requestFocus();
                hideKeyboardSearch();
            }
            @Override
            public void onSuggestionItemInserted(SuggestionItem item) {
                searchEditText.setText(item.query);
                searchEditText.setSelection(searchEditText.getText().length());
            }
            @Override
            public void onSuggestionItemLongClick(SuggestionItem item) {
                if (item.fromHistory) showDeleteSuggestionDialog(item);
            }
        });

        searchEditText.setOnFocusChangeListener(null);

        // show suggestions panel again when user starts typing
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                String newText = searchEditText.getText().toString();
                suggestionPublisher.onNext(newText);
                // here
                showSuggestionsPanel();
                itemsList.setVisibility(View.GONE);
            }
        };
        searchEditText.addTextChangedListener(textWatcher);

        searchEditText.setOnClickListener(v -> {
            if (!isSuggestionsEnabled) return;
            if (errorPanelRoot.getVisibility() == View.VISIBLE) return;
            itemsList.setVisibility(View.GONE);
            showSuggestionsPanel();
            showKeyboardSearch();
        });

        searchEditText.setOnEditorActionListener((TextView v, int actionId, KeyEvent event) -> {
            if (actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                hideKeyboardSearch();
                return false;
            }

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                search(searchEditText.getText().toString(), new String[0], "");
                return true;
            }

            return false;
        });
    }

    @Override
    protected void initListeners() {
        super.initListeners();
        infoListAdapter.setOnChannelSelectedListener(new OnClickGesture<ChannelInfoItem>() {
            @Override
            public void selected(ChannelInfoItem selectedItem) {
                try {
                    onItemSelected(selectedItem);
                    Navigator.openChannel(
                            getFM(),
                            selectedItem.getServiceId(),
                            selectedItem.getUrl(),
                            selectedItem.getName()
                    );
                } catch (Exception e) {
                    ErrorActivity.reportUiError((AppCompatActivity) getActivity(), e);
                }
            }
        });
        infoListAdapter.setOnPlaylistSelectedListener(new OnClickGesture<PlaylistInfoItem>() {
            @Override
            public void selected(PlaylistInfoItem selectedItem) {
                try {
                    onItemSelected(selectedItem);
                    Navigator.openPlaylist(
                            getFM(),
                            selectedItem.getServiceId(),
                            selectedItem.getUrl(),
                            selectedItem.getName()
                    );
                } catch (Exception e) {
                    ErrorActivity.reportUiError((AppCompatActivity) getActivity(), e);
                }
            }
        });
    }

    @Override
    protected void onStreamSelected(StreamInfoItem selectedItem) {
        Navigator.openVideoDetails(requireFragmentManager(),
                selectedItem.getName(), selectedItem.getUrl(), selectedItem.getServiceId());
    }

    @Override
    public boolean onBackPressed() {
        getFragmentManager().popBackStack();
        return true;
    }

    @Override
    public void handleSuggestions(@NonNull List<SuggestionItem> suggestions) {
        super.handleSuggestions(suggestions);
        if (suggestions.size() > 0 && itemsList.getVisibility() == View.VISIBLE) {
            itemsList.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleResult(@NonNull SearchInfo result) {
        super.handleResult(result);
        hideSuggestionsPanel();
        itemsList.setVisibility(View.VISIBLE);
    }

    @Override
    public void handleNextItems(ListExtractor.InfoItemsPage result) {
        super.handleNextItems(result);
        hideSuggestionsPanel();
        itemsList.setVisibility(View.VISIBLE);
    }

    @Override
    protected void showSuggestionsPanel() {
        suggestionsPanel.setVisibility(View.VISIBLE);
    }

    @Override
    protected void hideSuggestionsPanel() {
        suggestionsPanel.setVisibility(View.GONE);
    }

    protected void handleArguments() {
        Bundle arguments = getArguments();
        if (arguments == null) return;
        showToolbar = arguments.getBoolean(ARG_SHOW_TOOLBAR, false);
    }
}
