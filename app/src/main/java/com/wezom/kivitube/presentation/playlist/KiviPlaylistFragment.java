package com.wezom.kivitube.presentation.playlist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.wezom.kivitube.presentation.main.KiviMainActivity;
import com.wezom.kivitube.utils.Navigator;

import org.schabi.newpipe.R;
import org.schabi.newpipe.extractor.playlist.PlaylistInfo;
import org.schabi.newpipe.extractor.stream.StreamInfoItem;
import org.schabi.newpipe.fragments.list.playlist.PlaylistFragment;

public class KiviPlaylistFragment extends PlaylistFragment {

    public static KiviPlaylistFragment newInstance(int serviceId, String url, String name) {
        KiviPlaylistFragment instance = new KiviPlaylistFragment();
        instance.setInitialData(serviceId, url, name);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    protected void initViews(View rootView, Bundle savedInstanceState) {
        super.initViews(rootView, savedInstanceState);

        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(true, false, false);
        kiviMainActivity.setDrawerEnabled(false);

        headerPopupButton.setVisibility(View.GONE);
        headerBackgroundButton.setVisibility(View.GONE);
        headerRootLayout.findViewById(R.id.anchorLeft).setVisibility(View.GONE);
        headerRootLayout.findViewById(R.id.anchorRight).setVisibility(View.GONE);
    }

    @Override
    public void handleResult(@NonNull PlaylistInfo result) {
        super.handleResult(result);
        headerPlayAllButton.setOnClickListener(view ->
                Navigator.openPlayer(getContext(), getPlayQueue()));
    }

    @Override
    protected void onStreamSelected(StreamInfoItem selectedItem) {
        Navigator.openVideoDetails(
                getFM(),
                selectedItem.getName(),
                selectedItem.getUrl(),
                selectedItem.getServiceId()
        );
    }
}
