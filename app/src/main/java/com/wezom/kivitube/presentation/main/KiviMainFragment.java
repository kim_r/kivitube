package com.wezom.kivitube.presentation.main;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.wezom.kivitube.presentation.subs.SubsExtractorService;
import com.wezom.kivitube.utils.Navigator;
import com.wezom.kivitube.utils.Rx;
import com.wezom.kivitube.utils.bus.EventDrawer;
import com.wezom.kivitube.utils.bus.EventKey;
import com.wezom.kivitube.utils.bus.RxBus;

import org.schabi.newpipe.R;
import org.schabi.newpipe.databinding.FragmentKiviMainBinding;
import org.schabi.newpipe.extractor.NewPipe;
import org.schabi.newpipe.extractor.StreamingService;
import org.schabi.newpipe.extractor.exceptions.ExtractionException;
import org.schabi.newpipe.util.Constants;

import java.io.File;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.wezom.kivitube.common.Constants.RSS_FILENAME;
import static com.wezom.kivitube.common.Constants.SUBS_RSS_LINK;
import static org.schabi.newpipe.local.subscription.services.SubscriptionsImportService.INPUT_STREAM_MODE;
import static org.schabi.newpipe.local.subscription.services.SubscriptionsImportService.KEY_MODE;
import static org.schabi.newpipe.local.subscription.services.SubscriptionsImportService.KEY_VALUE;

public class KiviMainFragment extends Fragment {

    private static final int RC_WRITE_PERMISSION = 42;

    protected FragmentKiviMainBinding binding;
    protected CompositeDisposable disposables = new CompositeDisposable();
    protected String focusedViewName = "";
    protected SharedPreferences shared;
    protected Disposable subsCheckerDisposable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shared = PreferenceManager.getDefaultSharedPreferences(requireContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentKiviMainBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initPager();
        setupListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(false, false, false);
        kiviMainActivity.setDrawerEnabled(true);
        // hide fucking keyboard
        InputMethodManager imm = (InputMethodManager) requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binding.searchField.getWindowToken(), 0);
        // focus
        getSelectedTab().requestFocus();
//        focusedViewName = "TabView";
        // if we have write permission and it's time to update subs - go auth, download file, update
        checkPermissions();
        waitForSubs();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (subsCheckerDisposable != null && !subsCheckerDisposable.isDisposed())
            subsCheckerDisposable.dispose();
    }

    @Override
    public void onDestroy() {
        disposables.clear();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_WRITE_PERMISSION) return;
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            if (!shouldShowRequestPermissionRationale(permissions[0])) {
                new AlertDialog.Builder(requireContext())
                        .setMessage(R.string.storage_permission_denied)
                        .setPositiveButton(R.string.finish, (self, which) -> self.dismiss())
                        .create()
                        .show();
            }
            return;
        }
        updateSubsIfNeeded();
    }

    protected void initPager() {
        KiviMainPagerAdapter adapter = new KiviMainPagerAdapter(getChildFragmentManager());
        adapter.setContext(requireContext());
        binding.pager.setAdapter(adapter);
        binding.tabs.setupWithViewPager(binding.pager);
        // add margin between tabs
        for (int i = 0; i < binding.tabs.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabs.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 25, 0);
            tab.requestLayout();
        }
    }

    protected void setupListeners() {
        binding.searchField
                .setOnClickListener(v -> Navigator.openSearchScreen(requireFragmentManager()));

        // for debugging purposes
        binding.searchField.setOnTouchListener((v, event) -> v.performClick());

        View tab1 = binding.tabs.getTabAt(0).view;
        tab1.setOnFocusChangeListener((v, hasFocus) -> handleTabFocus(hasFocus, 0));

        View tab2 = binding.tabs.getTabAt(1).view;
        tab2.setOnFocusChangeListener((v, hasFocus) -> handleTabFocus(hasFocus, 1));

        View tab3 = binding.tabs.getTabAt(2).view;
        tab3.setOnFocusChangeListener((v, hasFocus) -> handleTabFocus(hasFocus, 2));

        disposables.add(RxBus.getInstance().listen(EventDrawer.class).subscribe(event -> {
            if (!event.isDrawerOpened())
                getSelectedTab().requestFocus();
        }));

        disposables.add(RxBus.getInstance().listen(EventKey.class).subscribe(event -> {
            focusedViewName = event.getViewName();
        }));
    }

    protected View getSelectedTab() {
        return binding.tabs.getTabAt(binding.tabs.getSelectedTabPosition()).view;
    }

    protected boolean haveIGotHereFromAnotherTab() {
        return focusedViewName.equals("TabView"); // what you know about madness?
    }

    protected void handleTabFocus(boolean hasFocus, int tabIndex) {
        if (!hasFocus) return;
        if (haveIGotHereFromAnotherTab())
            binding.pager.setCurrentItem(tabIndex);
        else
            getSelectedTab().requestFocus();
    }

    protected void updateSubsIfNeeded() {
        long currTimestamp = System.currentTimeMillis();
        long prevTimestamp = shared.getLong("launch_time", 0L);

        int offset = TimeZone.getDefault().getOffset(currTimestamp);

        // days from 1970
        long currDay = (currTimestamp + offset) / 86_400_000;
        long prevDay = (prevTimestamp + offset) / 86_400_000;

        if (currDay <= prevDay) return;

        shared.edit().putLong("launch_time", currTimestamp).apply();
        Navigator.openDownloader(getFragmentManager(), SUBS_RSS_LINK);
    }

    protected void checkPermissions() {
        String write = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(requireContext(), write) == PackageManager.PERMISSION_GRANTED) {
            updateSubsIfNeeded();
        } else {
            requestPermissions(new String[]{write}, RC_WRITE_PERMISSION);
        }
    }

    protected void waitForSubs() {
        subsCheckerDisposable = Observable.interval(2, TimeUnit.SECONDS)
                .compose(Rx.applyBackgroundSchedulerToObservable())
                .subscribe(time -> {
                    File subsFile = getSubsRssFile();
                    if (subsFile == null) return;
                    extractSubsFromFile(subsFile);
                });
    }

    @Nullable protected File getSubsRssFile() {
        File downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        // rss file might have different names on different devices
        String rssFilename = null;
        for (String filename : downloads.list())
            if (filename.contains(RSS_FILENAME))
                rssFilename = filename;
        return rssFilename == null ? null : new File(downloads.getAbsolutePath() + "/" + rssFilename);
    }

    @Nullable protected Integer getServiceId() {
        try {
            StreamingService service = NewPipe.getService("YouTube");
            return service.getServiceId();
        } catch (ExtractionException e) {
            return null;
        }
    }

    protected void extractSubsFromFile(File file) {
        Intent fetchSubsIntent = new Intent(requireContext(), SubsExtractorService.class);
        fetchSubsIntent.putExtra(KEY_MODE, INPUT_STREAM_MODE);
        fetchSubsIntent.putExtra(KEY_VALUE, file.getAbsolutePath());
        fetchSubsIntent.putExtra(Constants.KEY_SERVICE_ID, getServiceId());
        requireContext().startService(fetchSubsIntent);
    }
}
