package com.wezom.kivitube.presentation.videodetails;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wezom.kivitube.presentation.main.KiviMainActivity;
import com.wezom.kivitube.utils.bus.EventKey;
import com.wezom.kivitube.utils.bus.RxBus;

import org.schabi.newpipe.R;
import org.schabi.newpipe.extractor.stream.StreamInfo;
import org.schabi.newpipe.fragments.detail.VideoDetailFragment;
import org.schabi.newpipe.report.UserAction;

import io.reactivex.disposables.Disposable;

public class KiviVideoDetailsFragment extends VideoDetailFragment {

    protected Disposable eventDisposable;

    public static KiviVideoDetailsFragment newInstance(int serviceId,
                                                       String videoUrl,
                                                       String name) {
        KiviVideoDetailsFragment instance = new KiviVideoDetailsFragment();
        instance.setInitialData(serviceId, videoUrl, name);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        setTitle("");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_kivi_video_detail, container, false);
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        eventDisposable = RxBus.getInstance().listen(EventKey.class).subscribe(event -> {
            if (event.getViewName().equals("NestedScrollView"))
                thumbnailBackgroundButton.requestFocus();
        });
    }

    @Override
    public void onDestroy() {
        if (eventDisposable != null) eventDisposable.dispose();
        super.onDestroy();
    }

    @Override
    protected void initViews(View rootView, Bundle savedInstanceState) {
        KiviMainActivity kiviMainActivity = (KiviMainActivity) this.activity;
        kiviMainActivity.setToolbarVisibility(true, false, true);
        kiviMainActivity.setDrawerEnabled(false);

        rootView.findViewById(R.id.detail_control_panel).setVisibility(View.GONE);
        rootView.findViewById(R.id.detail_thumbnail_root_layout).requestFocus();

        super.initViews(rootView, savedInstanceState);

        videoTitleToggleArrow.setVisibility(View.GONE); // don't show arrow
    }

    @Override
    protected void initListeners() {
        super.initListeners();
        videoTitleRoot.setOnClickListener(null); // don't open description
    }

    @Override
    public void handleResult(@NonNull StreamInfo info) {
        super.handleResult(info);

        videoTitleToggleArrow.setVisibility(View.GONE); // don't show arrow

        boolean showRelatedVideos = PreferenceManager.getDefaultSharedPreferences(activity)
                .getBoolean(getString(R.string.show_next_video_key), true);

        if (!showRelatedVideos) return;

        KiviRelatedVideosFragment instance = KiviRelatedVideosFragment.newInstance(currentInfo);

        if (null == relatedStreamsLayout) { // phone
            pageAdapter.updateItem(RELATED_TAB_TAG, instance);
            pageAdapter.notifyDataSetUpdate();
        } else { // tablet
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.relatedStreamsLayout, instance);
            transaction.commitNow();
            relatedStreamsLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showSnackBarError(Throwable exception, UserAction userAction, String serviceName,
                                  String request, int errorId) {
        // do nothing, customer doesn't want it
    }
}
