package com.wezom.kivitube.presentation.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.wezom.kivitube.utils.Navigator;
import com.wezom.kivitube.utils.Rx;
import com.wezom.kivitube.utils.bus.EventDrawer;
import com.wezom.kivitube.utils.bus.EventKey;
import com.wezom.kivitube.utils.bus.RxBus;

import org.schabi.newpipe.R;
import org.schabi.newpipe.databinding.ActivityKiviMainBinding;
import org.schabi.newpipe.settings.SettingsActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

public class KiviMainActivity extends AppCompatActivity {

    protected ActivityKiviMainBinding binding;
    protected Disposable exitTimer;
    protected View prevFocusedView;
    protected boolean isUserWannaExit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.DarkTheme);
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_kivi_main);

        setupToolbar();
        setupDrawer();
        initScreen();
    }

    @Override
    protected void onDestroy() {
        if (exitTimer != null && !exitTimer.isDisposed())
            exitTimer.dispose();
        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
//            prevFocusedView = getCurrentFocus();
            return super.dispatchKeyEvent(event);
        }

        boolean isRightPressed = event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT;
        boolean isLeftPressed = event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;

        // close drawer by right dpad button
        if (isDrawerOpened() && isRightPressed)
            setDrawerOpened(false);

        // open drawer by left dpad button, when nowhere to set focus
        View curFocusedView = getCurrentFocus();
        if (!isDrawerOpened() && isLeftPressed && curFocusedView == prevFocusedView)
            setDrawerOpened(true);

        // crutch that fix problem with wrong focus transition (from video thumbnail to tab)
        if (isLeftPressed && name(prevFocusedView).equals("kivi_video_item") && name(curFocusedView).equals("TabView")) {
            setDrawerOpened(true);
        }

//        Toast.makeText(this, String.format("%s -> %s", name(prevFocusedView), name(curFocusedView)), Toast.LENGTH_SHORT).show();

        prevFocusedView = curFocusedView;

        RxBus.getInstance().publish(new EventKey(event.getKeyCode(), curFocusedView.getClass().getSimpleName()));

        return super.dispatchKeyEvent(event);
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpened()) {
            setDrawerOpened(false);
            return;
        }

        int stackSize = getSupportFragmentManager().getBackStackEntryCount();
        if (stackSize > 0) {
            super.onBackPressed();
            return;
        }

        if (isUserWannaExit) super.onBackPressed();
        else {
            isUserWannaExit = true;
            takeUserTimeToThink();
            Toast.makeText(this, R.string.exit_hint, Toast.LENGTH_SHORT).show();
        }
    }

    public void setToolbarVisibility(boolean isToolbarVisible,
                                     boolean isSearchContainerVisible,
                                     boolean isSpinnerVisible) {
        binding.toolbar.setVisibility(isToolbarVisible ? View.VISIBLE : View.GONE);
        binding.toolbarSpinner.setVisibility(isSpinnerVisible ? View.VISIBLE : View.GONE);
        binding.toolbarSearchContainer.setVisibility(isSearchContainerVisible ? View.VISIBLE : View.GONE);
    }

    public void setDrawerOpened(boolean shouldBeOpened) {
        int lockMode = binding.drawerLayout.getDrawerLockMode(Gravity.START);
        if (lockMode == DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            return;
        if (shouldBeOpened) {
            binding.drawerLayout.openDrawer(Gravity.START);
            binding.drawerContent.searchButton.requestFocus();
            RxBus.getInstance().publish(new EventDrawer(true));
        } else {
            binding.drawerLayout.closeDrawer(Gravity.START);
            RxBus.getInstance().publish(new EventDrawer(false));
        }
    }

    public boolean isDrawerOpened() {
        return binding.drawerLayout.isDrawerOpen(Gravity.START);
    }

    public void setDrawerEnabled(boolean isDrawerEnabled) {
        int lockMode = isDrawerEnabled
                ? DrawerLayout.LOCK_MODE_UNLOCKED
                : DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        binding.drawerLayout.setDrawerLockMode(lockMode, Gravity.START);
    }

    protected void initScreen() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new KiviMainFragment())
                .commit();
    }

    protected void setupToolbar() {
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setNavigationOnClickListener(v ->
                getSupportFragmentManager().popBackStack());
    }

    protected void setupDrawer() {
        binding.drawerContent.searchButton.setOnClickListener(v -> {
            Navigator.openSearchScreen(getSupportFragmentManager());
            setDrawerOpened(false);
            setDrawerEnabled(false);
        });
        binding.drawerContent.trends.setOnClickListener(v -> {
            Navigator.openTrends(getSupportFragmentManager());
            setDrawerOpened(false);
            setDrawerEnabled(false);
        });
        binding.drawerContent.subs.setOnClickListener(v -> {
            Navigator.openSubs(getSupportFragmentManager());
            setDrawerOpened(false);
            setDrawerEnabled(false);
        });
        binding.drawerContent.history.setOnClickListener(v -> {
            Navigator.openHistory(getSupportFragmentManager());
            setDrawerOpened(false);
            setDrawerEnabled(false);
        });
        binding.drawerContent.settings.setOnClickListener(v -> {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            setDrawerOpened(false);
            setDrawerEnabled(false);
        });
    }

    protected void makeContentMovableByDrawer() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                float slideX = drawerView.getWidth() * slideOffset;
                binding.content.setTranslationX(slideX);
//                binding.content.setScaleX(1 - slideOffset);
//                binding.content.setScaleY(1 - slideOffset);
            }
        };
        binding.drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    protected void takeUserTimeToThink() {
        exitTimer = Single.timer(2, TimeUnit.SECONDS)
                .compose(Rx.applyBackgroundScheduler())
                .subscribe((time, throwable) -> isUserWannaExit = false);
    }

    private String name(View v) {
        try {
            return getResources().getResourceEntryName(v.getId());
        } catch(Exception ex) {
            return v.getClass().getSimpleName();
        }
    }
}
