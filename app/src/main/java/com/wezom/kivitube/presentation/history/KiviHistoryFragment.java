package com.wezom.kivitube.presentation.history;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.wezom.kivitube.presentation.main.KiviMainActivity;
import com.wezom.kivitube.utils.Navigator;

import org.schabi.newpipe.database.LocalItem;
import org.schabi.newpipe.database.stream.StreamStatisticsEntry;
import org.schabi.newpipe.local.history.StatisticsPlaylistFragment;
import org.schabi.newpipe.util.OnClickGesture;

import java.util.List;

public class KiviHistoryFragment extends StatisticsPlaylistFragment {

    private static final String ARG_SHOW_TOOLBAR = "ARG_SHOW_TOOLBAR";

    protected boolean showToolbar;
    protected boolean isSortButtonPressed;

    public static KiviHistoryFragment newInstance(boolean showToolbar) {
        KiviHistoryFragment instance = new KiviHistoryFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_SHOW_TOOLBAR, showToolbar);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleArguments();
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(showToolbar, false, false);
        kiviMainActivity.setDrawerEnabled(!showToolbar);
        headerBackgroundButton.setVisibility(View.GONE);
        headerPlayAllButton.setVisibility(View.GONE);
        headerPopupButton.setVisibility(View.GONE);
    }

    @Override
    protected void initListeners() {
        super.initListeners();
        itemListAdapter.setSelectedListener(new OnClickGesture<LocalItem>() {
            @Override
            public void selected(LocalItem selectedItem) {
                if (selectedItem instanceof StreamStatisticsEntry) {
                    final StreamStatisticsEntry item = (StreamStatisticsEntry) selectedItem;
                    Navigator.openVideoDetails(getFM(), item.title, item.url, item.serviceId);
                }
            }

            @Override
            public void held(LocalItem selectedItem) {
                // don't show dialog
            }
        });
    }

    @Override
    public void handleResult(@NonNull List<StreamStatisticsEntry> result) {
        super.handleResult(result);
        // focus is lost otherwise
        if (isSortButtonPressed) {
            sortButton.requestFocus();
            isSortButtonPressed = false;
        }
    }

    @Override
    protected void toggleSortMode() {
        super.toggleSortMode();
        isSortButtonPressed = true;
    }

    protected void handleArguments() {
        if (getArguments() == null) return;
        showToolbar = getArguments().getBoolean(ARG_SHOW_TOOLBAR, false);
    }
}
