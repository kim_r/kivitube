package com.wezom.kivitube.presentation.main;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wezom.kivitube.presentation.history.KiviHistoryFragment;
import com.wezom.kivitube.presentation.subs.KiviSubsFragment;
import com.wezom.kivitube.presentation.trends.KiviTrendsFragment;

import org.schabi.newpipe.R;

import java.lang.ref.WeakReference;

public class KiviMainPagerAdapter extends FragmentPagerAdapter {

    private WeakReference<Context> context;

    KiviMainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return KiviTrendsFragment.newInstance(false);
            case 1:
                return KiviSubsFragment.newInstance(false);
            case 2:
                return KiviHistoryFragment.newInstance(false);
            default:
                return new Fragment();
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (context.get() == null)
            return super.getPageTitle(position);

        switch (position) {
            case 0:
                return context.get().getString(R.string.kivi_trends);
            case 1:
                return context.get().getString(R.string.tab_subscriptions);
            case 2:
                return context.get().getString(R.string.action_history);
        }

        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    public void setContext(Context context) {
        this.context = new WeakReference<>(context);
    }
}
