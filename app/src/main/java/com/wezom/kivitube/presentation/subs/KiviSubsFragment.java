package com.wezom.kivitube.presentation.subs;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.wezom.kivitube.presentation.main.KiviMainActivity;
import com.wezom.kivitube.utils.Navigator;
import com.wezom.kivitube.utils.Rx;

import org.schabi.newpipe.R;
import org.schabi.newpipe.database.subscription.SubscriptionEntity;
import org.schabi.newpipe.extractor.NewPipe;
import org.schabi.newpipe.extractor.StreamingService;
import org.schabi.newpipe.extractor.channel.ChannelInfoItem;
import org.schabi.newpipe.extractor.exceptions.ExtractionException;
import org.schabi.newpipe.local.subscription.SubscriptionFragment;
import org.schabi.newpipe.util.Constants;
import org.schabi.newpipe.util.OnClickGesture;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

import static com.wezom.kivitube.common.Constants.RSS_FILENAME;
import static com.wezom.kivitube.common.Constants.SUBS_RSS_LINK;
import static org.schabi.newpipe.local.subscription.services.SubscriptionsImportService.INPUT_STREAM_MODE;
import static org.schabi.newpipe.local.subscription.services.SubscriptionsImportService.KEY_MODE;
import static org.schabi.newpipe.local.subscription.services.SubscriptionsImportService.KEY_VALUE;

public class KiviSubsFragment extends SubscriptionFragment {

    private static final String ARG_SHOW_TOOLBAR = "ARG_SHOW_TOOLBAR";
    private static final int RC_WRITE_PERMISSION = 42;

    protected boolean showToolbar;
    protected boolean isTimeToGetFile;
    protected String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    protected Disposable fileCheckerDisposable;
    protected AlertDialog downloadDialog;
    protected AlertDialog extractDialog;

    public static KiviSubsFragment newInstance(boolean showToolbar) {
        KiviSubsFragment instance = new KiviSubsFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_SHOW_TOOLBAR, showToolbar);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleArguments();
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(showToolbar, false, false);
        kiviMainActivity.setDrawerEnabled(!showToolbar);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (hasWritePermission() && isTimeToGetFile) {
            waitForRssFile();
            isTimeToGetFile = false;
        }
    }

    @Override
    public void onPause() {
        if (fileCheckerDisposable != null && !fileCheckerDisposable.isDisposed())
            fileCheckerDisposable.dispose();
        if (downloadDialog != null) downloadDialog.dismiss();
        if (extractDialog != null) extractDialog.dismiss();
        super.onPause();
    }

    @Override
    protected void initViews(View rootView, Bundle savedInstanceState) {
        super.initViews(rootView, savedInstanceState);
        importExportListHeader.findViewById(R.id.import_export_expand_icon).setVisibility(View.INVISIBLE);
    }

    @Override
    protected void initListeners() {
        infoListAdapter.setOnChannelSelectedListener(new OnClickGesture<ChannelInfoItem>() {

            public void selected(ChannelInfoItem selectedItem) {
                Navigator.openChannel(getFM(), selectedItem.getServiceId(),
                        selectedItem.getUrl(), selectedItem.getName());
            }

            public void held(ChannelInfoItem selectedItem) {
                // do not show dialog
            }
        });

        whatsNewItemListHeader.setOnClickListener(null);

        importExportListHeader.setOnClickListener(v -> {
            if (hasWritePermission()) downloadOrExtractSubs();
            else requestPermissions(permissions, RC_WRITE_PERMISSION);
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != RC_WRITE_PERMISSION) return;
        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            if (!shouldShowRequestPermissionRationale(permissions[0])) {
                new AlertDialog.Builder(requireContext())
                        .setMessage(R.string.storage_permission_denied)
                        .setPositiveButton(R.string.finish, (self, which) -> self.dismiss())
                        .create()
                        .show();
            }
            return;
        }

        downloadOrExtractSubs();
    }

    @Override
    public void handleResult(@NonNull List<SubscriptionEntity> result) {
        super.handleResult(result);
        whatsNewItemListHeader.setVisibility(View.GONE);
    }

    protected void handleArguments() {
        if (getArguments() == null) return;
        showToolbar = getArguments().getBoolean(ARG_SHOW_TOOLBAR, false);
    }

    @Nullable
    protected Integer getServiceId() {
        try {
            StreamingService service = NewPipe.getService("YouTube");
            return service.getServiceId();
        } catch (ExtractionException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    protected boolean hasWritePermission() {
        return ContextCompat.checkSelfPermission(
                requireContext(),
                permissions[0]
        ) == PackageManager.PERMISSION_GRANTED;
    }

    @Nullable
    protected File getSubsRssFile() {
        File downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        // rss file might have different names on different devices
        String rssFilename = null;
        for (String filename : downloads.list())
            if (filename.contains(RSS_FILENAME))
                rssFilename = filename;
        return rssFilename == null ? null : new File(downloads.getAbsolutePath() + "/" + rssFilename);
    }

    protected void askToDownloadFileViaBrowser() {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(requireContext())
//                .setMessage(R.string.download_subs_hint)
//                .setCancelable(false)
//                .setNegativeButton(R.string.cancel, (self, which) -> self.dismiss());
//
//        dialog.setPositiveButton(R.string.finish, (self, which) -> {
////            Intent fetchSubsRssFileIntent = new Intent(Intent.ACTION_VIEW);
////            fetchSubsRssFileIntent.setData(Uri.parse(SUBS_RSS_LINK));
////            startActivity(fetchSubsRssFileIntent);
//            Navigator.openDownloader(getFM(), SUBS_RSS_LINK);
//            isTimeToGetFile = true;
//            self.dismiss();
//        });
//
//        downloadDialog = dialog.create();
//        downloadDialog.show();
        Navigator.openDownloader(getFM(), SUBS_RSS_LINK);
        isTimeToGetFile = true;
    }

    protected void askToExtractSubsFromFile(File subsRssFile) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(requireContext())
                .setMessage(R.string.extract_subs_hint);

        dialog.setPositiveButton(R.string.extract, (self, which) -> {
            extractSubsFromFile(subsRssFile);
            self.dismiss();
        });

        dialog.setNegativeButton(R.string.delete, (self, which) -> {
            subsRssFile.delete();
            self.dismiss();
        });

        extractDialog = dialog.create();
        extractDialog.show();
    }

    protected void extractSubsFromFile(File file) {
        Intent fetchSubsIntent = new Intent(activity, SubsExtractorService.class);
        fetchSubsIntent.putExtra(KEY_MODE, INPUT_STREAM_MODE);
        fetchSubsIntent.putExtra(KEY_VALUE, file.getAbsolutePath());
        fetchSubsIntent.putExtra(Constants.KEY_SERVICE_ID, getServiceId());
        requireContext().startService(fetchSubsIntent);
    }

    protected void downloadOrExtractSubs() {
        File subsFile = getSubsRssFile();
        if (subsFile == null) askToDownloadFileViaBrowser();
        else askToExtractSubsFromFile(subsFile);
    }

    protected void waitForRssFile() {
        fileCheckerDisposable = Observable.interval(2, TimeUnit.SECONDS)
                .compose(Rx.applyBackgroundSchedulerToObservable())
                .subscribe(time -> {
                    File subsFile = getSubsRssFile();
                    if (subsFile == null) return;
                    extractSubsFromFile(subsFile);
                });
    }
}
