package com.wezom.kivitube.presentation.subs;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.schabi.newpipe.R;
import org.schabi.newpipe.database.subscription.SubscriptionEntity;
import org.schabi.newpipe.local.subscription.services.SubscriptionsImportService;

import java.io.File;
import java.util.List;

import static org.schabi.newpipe.MainActivity.DEBUG;

public class SubsExtractorService extends SubscriptionsImportService {

    protected String subsFilePath;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        subsFilePath = intent.getStringExtra(KEY_VALUE);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected Subscriber<List<SubscriptionEntity>> getSubscriber() {
        return new Subscriber<List<SubscriptionEntity>>() {

            @Override
            public void onSubscribe(Subscription s) {
                subscription = s;
                s.request(Long.MAX_VALUE);
            }

            @Override
            public void onNext(List<SubscriptionEntity> successfulInserted) {
                if (DEBUG) Log.d(TAG, "startImport() " + successfulInserted.size() + " items successfully inserted into the database");
            }

            @Override
            public void onError(Throwable error) {
                handleError(error);
            }

            @Override
            public void onComplete() {
                LocalBroadcastManager.getInstance(SubsExtractorService.this).sendBroadcast(new Intent(IMPORT_COMPLETE_ACTION));
                showToast(R.string.import_complete_toast);
                removeSubsFile();
                stopService();
            }
        };
    }

    protected void removeSubsFile() {
        File file = new File(subsFilePath);
        boolean wasDeleted = file.delete();
//        Toast.makeText(this, wasDeleted ? "File was successfully deleted!" : "File wasn't deleted!", Toast.LENGTH_SHORT).show();
    }
}
