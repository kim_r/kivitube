package com.wezom.kivitube.presentation.subs;

import android.app.DownloadManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wezom.kivitube.presentation.main.KiviMainActivity;

import org.schabi.newpipe.BaseFragment;
import org.schabi.newpipe.R;
import org.schabi.newpipe.databinding.FragmentSubsDownloaderBinding;

import static android.content.Context.DOWNLOAD_SERVICE;

public class SubsDownloaderFragment extends BaseFragment {

    protected static final String ARG_URL = "ARG_URL";

    protected FragmentSubsDownloaderBinding binding;

    public static SubsDownloaderFragment newInstance(String url) {
        SubsDownloaderFragment instance = new SubsDownloaderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL, url);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentSubsDownloaderBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(true, false, false);
        kiviMainActivity.setDrawerEnabled(false);

        setTitle(getString(R.string.kivi_login));

        showLoading(true);

        prepareWebView();
        loadPage();
    }

    protected void prepareWebView() {
        binding.browser.getSettings().setJavaScriptEnabled(true);

        binding.browser.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d("web-view", "page finished: " + url);
                // hide loading if we got login page
                if (url.startsWith("https://accounts.google.com"))
                    showLoading(false);
            }
        });

        binding.browser.setDownloadListener((downloadUrl, userAgent, contentDisposition, mimeType, contentLength) -> {
            showLoading(true);
            Log.d("web-view", "download " + downloadUrl);

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(downloadUrl));
            request.setMimeType(mimeType);
            String cookies = CookieManager.getInstance().getCookie(downloadUrl);
            request.addRequestHeader("cookie", cookies);
            request.addRequestHeader("User-Agent", userAgent);
            request.setDescription("Downloading subscriptions");
            request.setTitle(URLUtil.guessFileName(downloadUrl, contentDisposition, mimeType));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS,
                    URLUtil.guessFileName(downloadUrl, contentDisposition, mimeType)
            );

            DownloadManager dm = (DownloadManager) requireActivity().getSystemService(DOWNLOAD_SERVICE);
            if (dm == null) return;
            dm.enqueue(request);

            getFragmentManager().popBackStack();
        });
    }

    protected void loadPage() {
        if (getArguments() == null) return;
        String url = getArguments().getString(ARG_URL, "");
        binding.browser.loadUrl(url);
    }

    protected void showLoading(boolean loading) {
        if (loading) {
            binding.loader.setVisibility(View.VISIBLE);
            binding.browser.setVisibility(View.INVISIBLE);
        } else {
            binding.loader.setVisibility(View.GONE);
            binding.browser.setVisibility(View.VISIBLE);
        }
    }

    protected void fuckBrowserData() {
        WebStorage.getInstance().deleteAllData();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        }
        binding.browser.clearCache(true);
        binding.browser.clearFormData();
        binding.browser.clearHistory();
    }
}
