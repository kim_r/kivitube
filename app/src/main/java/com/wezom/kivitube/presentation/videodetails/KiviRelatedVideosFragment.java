package com.wezom.kivitube.presentation.videodetails;

import android.support.v4.app.FragmentManager;

import com.wezom.kivitube.utils.Navigator;

import org.schabi.newpipe.extractor.stream.StreamInfo;
import org.schabi.newpipe.extractor.stream.StreamInfoItem;
import org.schabi.newpipe.fragments.list.videos.RelatedVideosFragment;

public class KiviRelatedVideosFragment extends RelatedVideosFragment {

    public static KiviRelatedVideosFragment newInstance(StreamInfo info) {
        KiviRelatedVideosFragment instance = new KiviRelatedVideosFragment();
        instance.setInitialData(info);
        return instance;
    }

    @Override
    protected void onStreamSelected(StreamInfoItem selectedItem) {
        FragmentManager fm = requireActivity().getSupportFragmentManager();
        Navigator.openVideoDetails(fm, selectedItem.getName(), selectedItem.getUrl(), selectedItem.getServiceId());
    }
}
