package com.wezom.kivitube.presentation.trends;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;

import com.wezom.kivitube.presentation.main.KiviMainActivity;
import com.wezom.kivitube.utils.Navigator;

import org.schabi.newpipe.extractor.NewPipe;
import org.schabi.newpipe.extractor.StreamingService;
import org.schabi.newpipe.extractor.exceptions.ExtractionException;
import org.schabi.newpipe.extractor.kiosk.KioskInfo;
import org.schabi.newpipe.extractor.stream.StreamInfoItem;
import org.schabi.newpipe.extractor.utils.Localization;
import org.schabi.newpipe.fragments.list.kiosk.KioskFragment;

import io.reactivex.Single;

public class KiviTrendsFragment extends KioskFragment {

    private static final String ARG_SHOW_TOOLBAR = "ARG_SHOW_TOOLBAR";

    protected boolean showToolbar;
    protected Localization savedLocalization;

    public static KiviTrendsFragment newInstance(boolean showToolbar) {
        int serviceId = 0;
        String kioskId = "Trending";
        String url = "";

        try {
            kioskId = NewPipe.getService(serviceId).getKioskList().getDefaultKioskId();
            StreamingService service = NewPipe.getService(serviceId);
            url = service
                    .getKioskList()
                    .getListLinkHandlerFactoryByType(kioskId)
                    .fromId(kioskId)
                    .getUrl();
        } catch (ExtractionException e) {
            Log.e("KiviTrendsFragment", e.getMessage());
        }

        KiviTrendsFragment instance = new KiviTrendsFragment();
        instance.kioskId = kioskId;
        instance.setInitialData(serviceId, url, kioskId);

        Bundle args = new Bundle();
        args.putBoolean(ARG_SHOW_TOOLBAR, showToolbar);
        instance.setArguments(args);

        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleArguments();
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        KiviMainActivity kiviMainActivity = (KiviMainActivity) requireActivity();
        kiviMainActivity.setToolbarVisibility(showToolbar, false, false);
        kiviMainActivity.setDrawerEnabled(!showToolbar);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkLocalizationChanges();
    }

    @Override
    protected void onStreamSelected(StreamInfoItem selectedItem) {
        FragmentManager fm = requireActivity().getSupportFragmentManager();
        // it would be nice to use cicerone instead of this dick
        Navigator.openVideoDetails(fm, selectedItem.getName(), selectedItem.getUrl(), selectedItem.getServiceId());
    }

    @Override
    protected void showStreamDialog(StreamInfoItem item) {
        // do nothing, I don't need dialog
    }

    @Override
    public Single<KioskInfo> loadResult(boolean forceReload) {
        return super.loadResult(true); // always fetch data from the internet
    }

    protected void handleArguments() {
        if (getArguments() == null) return;
        showToolbar = getArguments().getBoolean(ARG_SHOW_TOOLBAR, false);
    }

    protected void checkLocalizationChanges() {
        Localization freshLocalization = NewPipe.getPreferredLocalization();

        if (savedLocalization == null) {
            savedLocalization = freshLocalization;
            return;
        }

        boolean sameCountry = freshLocalization.getCountry().equals(savedLocalization.getCountry());
        boolean sameLang = freshLocalization.getLanguage().equals(savedLocalization.getLanguage());
        if (!sameCountry || !sameLang) {
            // because fragment cannot handle result if there are some items in adapter
            // spectacular api
            infoListAdapter.clearStreamItemList();
            // force reload from internet (actually parameter is overwritten in loadResult method)
            startLoading(true);
        }

        savedLocalization = freshLocalization;
    }
}
