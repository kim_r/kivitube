package com.wezom.kivitube.utils.bus;

public class EventKey {

    private int keyCode;
    private String viewName;

    public EventKey(int keyCode, String viewName) {
        this.keyCode = keyCode;
        this.viewName = viewName;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public String getViewName() {
        return viewName;
    }
}
