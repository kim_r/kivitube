package com.wezom.kivitube.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.wezom.kivitube.presentation.channel.KiviChannelFragment;
import com.wezom.kivitube.presentation.history.KiviHistoryFragment;
import com.wezom.kivitube.presentation.playlist.KiviPlaylistFragment;
import com.wezom.kivitube.presentation.search.KiviSearchFragment;
import com.wezom.kivitube.presentation.subs.KiviSubsFragment;
import com.wezom.kivitube.presentation.subs.SubsDownloaderFragment;
import com.wezom.kivitube.presentation.trends.KiviTrendsFragment;
import com.wezom.kivitube.presentation.videodetails.KiviVideoDetailsFragment;

import org.schabi.newpipe.R;
import org.schabi.newpipe.player.MainVideoPlayer;
import org.schabi.newpipe.player.VideoPlayer;
import org.schabi.newpipe.player.playqueue.PlayQueue;
import org.schabi.newpipe.util.SerializedCache;

/**
 * Drop this evil monster and use cicerone or something else!
 */
public class Navigator {

    public static void openVideoDetails(FragmentManager fm,
                                        String title,
                                        String url,
                                        int serviceId) {
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment instanceof KiviVideoDetailsFragment && fragment.isVisible()) {
            KiviVideoDetailsFragment detailFragment = (KiviVideoDetailsFragment) fragment;
            detailFragment.setAutoplay(false);
            detailFragment.selectAndLoadVideo(serviceId, url, title);
            return;
        }

        KiviVideoDetailsFragment instance = KiviVideoDetailsFragment
                .newInstance(serviceId, url, title);
        instance.setAutoplay(false);

        navigateTo(fm, instance);
    }

    public static void openSearchScreen(FragmentManager fm) {
        KiviSearchFragment instance = KiviSearchFragment.newInstance(false);
        navigateTo(fm, instance);
    }

    public static void openTrends(FragmentManager fm) {
        KiviTrendsFragment instance = KiviTrendsFragment.newInstance(true);
        navigateTo(fm, instance);
    }

    public static void openSubs(FragmentManager fm) {
        KiviSubsFragment instance = KiviSubsFragment.newInstance(true);
        navigateTo(fm, instance);
    }

    public static void openHistory(FragmentManager fm) {
        KiviHistoryFragment instance = KiviHistoryFragment.newInstance(true);
        navigateTo(fm, instance);
    }

    public static void openChannel(FragmentManager fm, int serviceId, String url, String name) {
        KiviChannelFragment instance = KiviChannelFragment.newInstance(serviceId, url, name);
        navigateTo(fm, instance);
    }

    public static void openDownloader(FragmentManager fm, String url) {
        SubsDownloaderFragment instance = SubsDownloaderFragment.newInstance(url);
        navigateTo(fm, instance);
    }

    public static void openPlaylist(FragmentManager fm, int serviceId, String url, String name) {
        KiviPlaylistFragment instance = KiviPlaylistFragment.newInstance(serviceId, url, name);
        navigateTo(fm, instance);
    }

    public static void openPlayer(Context context, PlayQueue queue) {
        if (context == null) return;

        Intent player = new Intent(context, MainVideoPlayer.class);

        String cacheKey = SerializedCache.getInstance().put(queue, PlayQueue.class);
        if (cacheKey != null) player.putExtra(VideoPlayer.PLAY_QUEUE_KEY, cacheKey);

        player.putExtra(VideoPlayer.RESUME_PLAYBACK, false);
        player.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(player);
    }

    private static void navigateTo(FragmentManager fragmentManager, Fragment instance) {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, instance)
                .addToBackStack(null)
                .commit();
    }
}
