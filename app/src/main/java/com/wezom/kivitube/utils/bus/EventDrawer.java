package com.wezom.kivitube.utils.bus;

public class EventDrawer {

    private boolean isOpened;

    public EventDrawer(boolean isOpened) {
        this.isOpened = isOpened;
    }

    public boolean isDrawerOpened() {
        return isOpened;
    }
}
