package com.wezom.kivitube.utils.bus;

import com.jakewharton.rxrelay2.PublishRelay;

import io.reactivex.Observable;

public class RxBus {

    private static RxBus instance;
    private PublishRelay<Object> publisher;

    public static RxBus getInstance() {
        if (instance == null) instance = new RxBus();
        return instance;
    }

    private RxBus() {
        publisher = PublishRelay.create();
    }

    public void publish(Object event) {
        publisher.accept(event);
    }

    public <T> Observable<T> listen(Class<T> eventType) {
        return publisher.ofType(eventType);
    }
}
