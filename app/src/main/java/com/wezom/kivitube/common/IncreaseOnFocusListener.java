package com.wezom.kivitube.common;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.schabi.newpipe.R;

public class IncreaseOnFocusListener implements View.OnFocusChangeListener {

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Animation animation = AnimationUtils.loadAnimation(v.getContext(),
                hasFocus ? R.anim.scale_up_video_item : R.anim.scale_out_video_item);
        v.startAnimation(animation);
        animation.setFillAfter(true);
    }
}
