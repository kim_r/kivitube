package com.wezom.kivitube.common;

public class Constants {
    public static final String AUTH_ENDPOINT = "https://accounts.google.com/o/oauth2/v2/auth";
    public static final String TOKEN_ENDPOINT = "https://www.googleapis.com/oauth2/v4/token";
    // todo change to their id
    public static final String OAUTH_CLIENT_ID = "453721705082-5t3qjvh5mq6h7pt3c4c7dabekjrkjec0.apps.googleusercontent.com";
    public static final String REDIRECT = "org.schabi.newpipe:/oauth2callback";
    public static final String[] SCOPES = new String[] {
            "https://www.googleapis.com/auth/userinfo.profile",
            "https://www.googleapis.com/auth/youtube.readonly"
    };
    public static final String SUBS_RSS_LINK = "https://www.youtube.com/subscription_manager?action_takeout=1";
    public static final String RSS_FILENAME = "subscription_manager";
}
